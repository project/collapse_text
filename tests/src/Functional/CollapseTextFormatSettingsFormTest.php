<?php

declare(strict_types=1);

namespace Drupal\Tests\collapse_text\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\filter\Entity\FilterFormat;

/**
 * Test Collapse Text settings on a text format editing form.
 *
 * @group collapse_text
 */
class CollapseTextFormatSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'collapse_text',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with the 'administer filters' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    // Create text format.
    $filtered_html_format = FilterFormat::create([
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
      'weight' => 0,
      'filters' => [],
    ]);
    $filtered_html_format->save();
    // Create admin user.
    $this->adminUser = $this->drupalCreateUser([
      'administer filters',
    ]);
  }

  /**
   * Tests configuring Collapse Text for an existing text format.
   */
  public function testCollapseTextSettingsForm() {
    /** @var \Drupal\Tests\WebAssert $assert */
    $assert = $this->assertSession();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/formats/manage/filtered_html');

    $filter_collapse_text_status = $this->xpath('//input[@name="filters[filter_collapse_text][status]"]');
    $filter_collapse_text_status_checked = $this->xpath('//input[@name="filters[filter_collapse_text][status]" and @checked="checked"]');
    $this->assertCount(1, $filter_collapse_text_status, 'The Collapse Text filter exists.');
    $this->assertCount(0, $filter_collapse_text_status_checked, 'The Collapse Text filter is disabled.');

    // Enable the Collapse Text filter.
    $edit = [
      'filters[filter_collapse_text][status]' => '1',
    ];
    $this->submitForm($edit, 'Save configuration');
    $assert->pageTextContains('The text format Filtered HTML has been updated.');

    $filter_collapse_text_status_checked = $this->xpath('//input[@name="filters[filter_collapse_text][status]" and @checked="checked"]');
    $this->assertCount(1, $filter_collapse_text_status_checked, 'The Collapse Text filter is enabled.');

  }

}
